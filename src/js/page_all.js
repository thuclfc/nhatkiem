$(document).ready(function () {
    // show hidden menu
    $('.navbar-toggler').on('click',function () {
        $('.navbar,.modal-navbar').toggleClass('show');
    });
    $('.navbar .close,.modal-navbar').on('click',function () {
        $('.navbar,.modal-navbar').removeClass('show');
    });

    $('.box-right .close').on('click',function(){
        $('.box-right').toggleClass('is-show');
    });

    // active navbar of page current
    var urlcurrent = window.location.href;
    $(".navbar-nav li a[href$='"+urlcurrent+"'],.nav-category li a[href$='"+urlcurrent+"']").addClass('active');
});
